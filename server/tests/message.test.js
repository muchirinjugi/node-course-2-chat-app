var expect = require('expect');
var {
	generateMessage,
	generateLocationMessage
} = require('./../utils/message');

describe('generateMessage', () => {
	it('should generate correct message object', () => {
		var from = 'Jen',
			text = 'some message',
			message = generateMessage(from, text);

		expect(typeof message.createdAt).toBe('number');
		expect(message).toMatchObject({
			from: from,
			text: text
		});
	});
});

describe('generateLocationMessage', () => {
	it('should generate correct location object', () => {
		var from = 'Jen',
			latitude = '1',
			longitude = '1',
			message = generateLocationMessage(from, latitude, longitude);

		expect(typeof message.createdAt).toBe('number');
		expect(message).toMatchObject({
			from: from,
			url: `https://www.google.com/maps?q=${latitude},${longitude}`
		});
	});
});
