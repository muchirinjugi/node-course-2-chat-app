const express = require('express');
const socketIO = require('socket.io');
const http = require('http');
const path = require('path');

const { generateMessage, generateLocationMessage } = require('./utils/message');

const publicPath = path.join(__dirname, '../public');
const port = process.env.PORT || 3000;

var app = express();
const server = http.createServer(app);
const io = socketIO(server);

io.on('connection', socket => {
	console.log('new user connected');

	/**
	 * @description socket.emit() emits a message to a single connection
	 * io.emit() emits a message to every single connection
	 */

	socket.emit(
		'newMessage',
		generateMessage('Admin', 'Welcome to the chat app')
	);
	socket.broadcast.emit(
		'newMessage',
		generateMessage('Admin', 'New user Joined')
	);

	socket.on('createMessage', (msg, callback) => {
		console.log('recieved message', msg);

		io.emit('newMessage', generateMessage(msg.from, msg.text));
		if (callback) callback();
	});

	socket.on('createLocationMessage', coords => {
		io.emit(
			'newLocationMessage',
			generateLocationMessage(
				'User',
				coords.latitude,
				coords.longitude
			)
		);
	});

	socket.on('disconnect', () => {
		console.log('client disconnected');
	});
});

app.use(express.static(publicPath));

server.listen(port, () => {
	console.log(`server is up on port ${port}`);
});
