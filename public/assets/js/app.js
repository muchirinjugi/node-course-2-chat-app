var socket = io();
socket.on('connect', () => console.log('connected to server'));

// selectors
var locationButton = document.getElementById('geoBtn');
var messageForm = document.getElementById('message-form');

//heights

var scrollToBottom = () => {
	var messagePanel = document.getElementById('messages');

	var lastMessageHeight = messagePanel.lastChild.clientHeight;
	var mClientHeight = messagePanel.clientHeight;
	var mScrolltop = messagePanel.scrollTop;
	var mScrollHeight = messagePanel.scrollHeight;

	if (mClientHeight + mScrolltop + lastMessageHeight >= mScrollHeight) {
		messagePanel.scrollTop = mScrollHeight;
	} else if (mScrollHeight > 0) {
		messagePanel.scrollTop = mScrollHeight;
	}
};

var messageTitle = (messageFrom, time) => {
	return `
			<div class='message__title'>
				<h4>${messageFrom}</h4>
				<span>${time}</span>
			</div>
	`;
};

var displayMessage = (typeOfMessage, msg) => {
	var li = document.createElement('li');
	var formattedTime = moment(msg.createdAt).format('h:mm a');
	if (typeOfMessage === 'regular') {
		msg.from === 'User'
			? (li.className = 'rightMessage')
			: (li.className = 'leftMessage');

		li.innerHTML = `
			${messageTitle(msg.from, formattedTime)}
			<div class='message__body'>
				<p>${msg.text}</p>
			</div>`;
		document.getElementById('messages').appendChild(li);
		scrollToBottom();
	}
	if (typeOfMessage === 'locationMessage') {
		li.className = 'rightMessage';
		li.innerHTML = `
			${messageTitle(msg.from, formattedTime)}
			<div class='message__body'>
				<a href="${msg.url}" target="_blank">My current location</a>
			</div>`;
		document.getElementById('messages').appendChild(li);
		scrollToBottom();
	}
};

var locationButtonState = property => {
	if (property === 'disabled') {
		locationButton.setAttribute('disabled', 'disabled');
		locationButton.textContent = 'Sending Location....';
	} else if (property === 'enabled') {
		locationButton.removeAttribute('disabled');
		locationButton.textContent = 'Send Location';
	}
};

socket.on('newMessage', msg => displayMessage('regular', msg));

socket.on('newLocationMessage', msg => displayMessage('locationMessage', msg));

messageForm.addEventListener('submit', e => {
	e.preventDefault();
	messageBox = document.querySelector('[name=message]');
	socket.emit(
		'createMessage',
		{
			from: 'User',
			text: messageBox.value
		},
		() => {
			messageBox.value = '';
		}
	);
});

locationButton.addEventListener('click', () => {
	if (!navigator.geolocation)
		return alert('browser doesnt support geoloaction');

	locationButtonState('disabled');

	navigator.geolocation.getCurrentPosition(
		position => {
			locationButtonState('enabled');
			socket.emit('createLocationMessage', {
				latitude: position.coords.latitude,
				longitude: position.coords.longitude
			});
		},
		err => {
			locationButtonState('enabled');
			alert('unable to fetch location');
		}
	);
});

socket.on('disconnect', () => console.log('disconnected from server'));
